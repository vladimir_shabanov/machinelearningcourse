import java.util.ArrayList;

/**
 * Created by Vladimir on 02.11.2016.
 */
public class PotentialFunctionsMethod implements Classificator {
    private ArrayList<Iris> learningSample;
    private ArrayList<Iris> targetSample;


    @Override
    public void learn(ArrayList<Iris> learingSample) {
        this.learningSample = learingSample;
    }

    private double distance(Iris irisA, Iris irisB) {
        return Math.sqrt( (irisA.getPetalLength() - irisB.getPetalLength()) * (irisA.getPetalLength() - irisB.getPetalLength()) +
                (irisA.getPetalWidth() - irisB.getPetalWidth()) * (irisA.getPetalWidth() - irisB.getPetalWidth()) +
                (irisA.getSepalLength() - irisB.getSepalLength()) * (irisA.getSepalLength() - irisB.getSepalLength()) +
                (irisA.getSepalWidth() - irisB.getSepalWidth()) * (irisA.getSepalWidth() - irisB.getSepalWidth()) );
    }

    public double getPotential(Iris a, Iris b) {
        return 1/(distance(a, b) * distance(a, b));
    }

    @Override
    public ArrayList<Iris> getTargetSample() {
        return this.targetSample;
    }

    @Override
    public void classificate(ArrayList<Iris> targetSample) {
        for (int i = 0; i < targetSample.size(); i++) {
            double ones = 0;
            double twos = 0;
            double threes = 0;

            for (int j = 0; j < learningSample.size(); j++) {
                double potential = getPotential(targetSample.get(i), learningSample.get(j));
                switch (learningSample.get(j).getIrisClass()) {
                    case 1:
                        ones += potential;
                        break;
                    case 2:
                        twos += potential;
                        break;
                    case 3:
                        threes += potential;
                        break;
                }
            }
/*
            System.out.println("Ones: " + ones);
            System.out.println("Twos: " + twos);
            System.out.println("Threes: " + threes);*/

            if (ones > twos) {
                if (ones > threes)
                    targetSample.get(i).setIrisClass(1);
                else
                    targetSample.get(i).setIrisClass(3);
            } else {
                if (twos > threes)
                    targetSample.get(i).setIrisClass(2);
                else
                    targetSample.get(i).setIrisClass(3);
            }
            //System.out.println(targetSample.get(i).getIrisClass());
        }
        this.targetSample = targetSample;
    }
}
