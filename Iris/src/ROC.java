import java.util.ArrayList;

/**
 * Created by Vladimir on 30.11.2016.
 */
public class ROC {
    private ArrayList<Iris> targetSample;
    private ArrayList<Iris> testSample;
    private ArrayList<Iris> learningSample;
    private int irisClass;
    private int positives;
    private int negatives;

    public ROC(int irisClass, ArrayList<Iris> learningSample, ArrayList<Iris> targetSample, ArrayList<Iris> testSample) {
        this.irisClass = irisClass;
        this.targetSample = targetSample;
        this.testSample = testSample;
        this.learningSample = learningSample;

        positives = 0;
        negatives = 0;

        for (int i = 0; i < testSample.size(); i++) {
            testSample.get(i).setIrisClass((testSample.get(i).getIrisClass() == irisClass)? 1 : 0);
            if (testSample.get(i).getIrisClass() == 1)
                positives++;
            else
                negatives++;
        }

    }

    private void flushTargetSample() {
        for (int i = 0; i < targetSample.size(); i++) {
            targetSample.get(i).setIrisClass(0);
        }
    }

    private double getTPR() {
        int tp = 0;
        for (int i = 0; i < targetSample.size(); i++) {
            if (testSample.get(i).getIrisClass() == 1 && targetSample.get(i).getIrisClass() == 1) {
                tp++;
            }
        }
        return tp/positives;
    }

    private double getFPR() {
        int fp = 0;
        for (int i = 0; i < targetSample.size(); i++) {
            if (testSample.get(i).getIrisClass() == 0 && targetSample.get(i).getIrisClass() == 1) {
                fp++;
            }
        }
        return fp/negatives;
    }


    public void calc() {
        for (double tr = 0.0; tr < 1.0; tr += 0.05) {
            LogisticRegression logisticRegression = new LogisticRegression(irisClass, tr);
            logisticRegression.learn(learningSample);
            logisticRegression.classificate(targetSample);
            System.out.println(getFPR() + ", " + getTPR());

            flushTargetSample();
        }
    }
}
