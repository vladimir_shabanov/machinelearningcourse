import java.util.ArrayList;

/**
 * Created by Vladimir on 28.11.2016.
 */
public class LogisticRegression implements Classificator {

    private double rate = 0.0001;
    private int irisClass;
    private double tr;

    public LogisticRegression(int irisClass, double tr){
        w1 = w2 = w3 = w4 = 0;
        this.irisClass = irisClass;
        this.tr = tr;
    }

    private double w1;
    private double w2;
    private double w3;
    private double w4;

    private ArrayList<Iris> targetSample;

    private final int ITERATIONS = 300;

    private double classify(Iris iris) {
        double a = iris.getSepalLength();
        double b = iris.getSepalWidth();
        double c = iris.getPetalLength();
        double d = iris.getPetalWidth();
        double logit = .0;

        logit += w1 * a + w2 * b + w3 * c + w4 * d;
        return sigmoid(logit);
    }

    private double sigmoid(double z) {
        return 1.0 / (1.0 + Math.exp(-z));
    }

    @Override
    public void classificate(ArrayList<Iris> targetSample) {
        for (int i = 0; i < targetSample.size(); i++) {
            double predicted = classify(targetSample.get(i));
            targetSample.get(i).setIrisClass((predicted >= tr)? 1 : 0);
            //System.out.println(targetSample.get(i).getIrisClass());
        }
        this.targetSample = targetSample;
    }

    @Override
    public ArrayList<Iris> getTargetSample() {
        return this.targetSample;
    }

    @Override
    public void learn(ArrayList<Iris> learingSample) {
        for (int i = 0; i < ITERATIONS; i++) {
            for (int j = 0; j < learingSample.size(); j++) {
                double predicted = classify(learingSample.get(j));
                int label = (learingSample.get(j).getIrisClass() == irisClass)? 1 : 0; //выбрали первый класс против всех

                w1 = w1 + rate * (label - predicted) * learingSample.get(j).getSepalLength();
                w2 = w2 + rate * (label - predicted) * learingSample.get(j).getSepalWidth();
                w3 = w3 + rate * (label - predicted) * learingSample.get(j).getPetalLength();
                w4 = w4 + rate * (label - predicted) * learingSample.get(j).getPetalWidth();
                //System.out.println("w1 = " + w1 + ", w2 = " + w2 + ", w3 = " + w3 + ", w4 = " + w4);
            }
        }
    }
}
