import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Vladimir on 02.11.2016.
 */
public class KNearestNeighbours implements Classificator {

    protected ArrayList<Iris> learingSample;
    protected ArrayList<Iris> targetSample;
    protected int k;

    @Override
    public ArrayList<Iris> getTargetSample() {
        return this.targetSample;
    }

    private double distance(Iris irisA, Iris irisB) {
        return Math.sqrt( (irisA.getPetalLength() - irisB.getPetalLength()) * (irisA.getPetalLength() - irisB.getPetalLength()) +
                (irisA.getPetalWidth() - irisB.getPetalWidth()) * (irisA.getPetalWidth() - irisB.getPetalWidth()) +
                (irisA.getSepalLength() - irisB.getSepalLength()) * (irisA.getSepalLength() - irisB.getSepalLength()) +
                (irisA.getSepalWidth() - irisB.getSepalWidth()) * (irisA.getSepalWidth() - irisB.getSepalWidth()) );
    }

    public void setK(int k) {
        this.k = k;
    }

    public int getK() {
        return k;
    }

    public KNearestNeighbours(int k) {
        this.k = k;
    }
    public KNearestNeighbours() {

    }

    int getAnswer(ArrayList<Iris> nearestNeighbours) {
        int ones = 0;
        int twos = 0;
        int threes = 0;
        for (int i = 0; i < nearestNeighbours.size(); i++) {
            if (nearestNeighbours.get(i).getIrisClass() == 1)
                ones++;
            if (nearestNeighbours.get(i).getIrisClass() == 2)
                twos++;
            if (nearestNeighbours.get(i).getIrisClass() == 3)
                threes++;
        }

        if (ones > twos) {
            if (ones > threes)
                return 1;
            else
                return 3;
        } else {
            if (twos > threes)
                return 2;
            else
                return 3;
        }

    }

    protected ArrayList<Iris> getNearestNeighbours(Iris currentIris) {
        ArrayList<Iris> nearestNeighbours = new ArrayList<Iris>();
        ArrayList<Iris> tempCopy = new ArrayList<Iris>(learingSample);
        for (int i = 0; i < k; i++) {
            nearestNeighbours.add(getNearestNeighbor(currentIris, tempCopy));
        }

        return nearestNeighbours;
    }

    protected Iris getNearestNeighbor(Iris currentIris, ArrayList<Iris> sample) {
        double minDistance = Double.MAX_VALUE;
        int index = 0;
        for (int i = 0; i < sample.size(); i++) {
            if (distance(currentIris, sample.get(i)) < minDistance) {
                minDistance = distance(currentIris, sample.get(i));
                index = i;
            }
        }
        Iris result = sample.get(index);
        sample.remove(index);
        return result;
    }

    @Override
    public void learn(ArrayList<Iris> learingSample) {
        this.learingSample = learingSample;
    }

    @Override
    public void classificate(ArrayList<Iris> targetSample) {
        for (int i = 0; i < targetSample.size(); i++) {
            ArrayList<Iris> nearestNeightbours = getNearestNeighbours(targetSample.get(i));
            targetSample.get(i).setIrisClass(getAnswer(nearestNeightbours));
            //System.out.println(targetSample.get(i).getIrisClass());
        }
        this.targetSample = targetSample;
    }
}
