import java.util.ArrayList;

/**
 * Created by Vladimir on 03.11.2016.
 */
public class ParzenWindowMethod implements Classificator  {
    private ArrayList<Iris> learningSample;
    private ArrayList<Iris> targetSample;
    private double h;


    private double distance(Iris irisA, Iris irisB) {
        return Math.sqrt( (irisA.getPetalLength() - irisB.getPetalLength()) * (irisA.getPetalLength() - irisB.getPetalLength()) +
                (irisA.getPetalWidth() - irisB.getPetalWidth()) * (irisA.getPetalWidth() - irisB.getPetalWidth()) +
                (irisA.getSepalLength() - irisB.getSepalLength()) * (irisA.getSepalLength() - irisB.getSepalLength()) +
                (irisA.getSepalWidth() - irisB.getSepalWidth()) * (irisA.getSepalWidth() - irisB.getSepalWidth()) );
    }

    private ArrayList<Iris> getNeighbours(Iris iris) {
        ArrayList<Iris> neighbours = new ArrayList<Iris>();
        for (int i = 0; i < learningSample.size(); i++) {
            if (distance(iris, learningSample.get(i)) <= h) {
                neighbours.add(learningSample.get(i));
            }
        }
        return neighbours;
    }

    public ParzenWindowMethod(double h) {
        this.h = h;
    }

    private double getWeight(Iris a, Iris b) {
        //System.out.println(gaussianKernel(distance(a, b) / h));
        return triangleKernel(distance(a, b) / h);
    }

    private double gaussianKernel(double value) {
        return Math.pow(2*Math.PI, -0.5) * Math.pow(Math.E, -0.5 * value * value);
    }

    private double rectangleKernel(double value) {
        return 0.5 * value;
    }

    private double triangleKernel(double value) {
        return 1.0 - Math.abs(value);
    }

    private double quartKernel(double value) {
        return (15 * (1 - value * value))/16;
    }

    @Override
    public ArrayList<Iris> getTargetSample() {
        return targetSample;
    }

    @Override
    public void learn(ArrayList<Iris> learingSample) {
        this.learningSample = learingSample;
    }

    @Override
    public void classificate(ArrayList<Iris> targetSample) {
        for (int i = 0; i < targetSample.size(); i++) {
            double ones = 0;
            double twos = 0;
            double threes = 0;

            ArrayList<Iris> neighbours = getNeighbours(targetSample.get(i));

            for (int j = 0; j < neighbours.size(); j++) {
                switch (neighbours.get(j).getIrisClass()) {
                    case 1:
                        ones += getWeight(targetSample.get(i), neighbours.get(j));
                        break;
                    case 2:
                        twos += getWeight(targetSample.get(i), neighbours.get(j));
                        break;
                    case 3:
                        threes += getWeight(targetSample.get(i), neighbours.get(j));
                        break;
                }
            }

           /* System.out.println("Ones: " + ones);
            System.out.println("Twos: " + twos);
            System.out.println("Threes: " + threes);*/


            if (ones > twos) {
                if (ones > threes)
                    targetSample.get(i).setIrisClass(1);
                else
                    targetSample.get(i).setIrisClass(3);
            } else {
                if (twos > threes)
                    targetSample.get(i).setIrisClass(2);
                else
                    targetSample.get(i).setIrisClass(3);
            }
            //System.out.println(targetSample.get(i).getIrisClass());

        }

        this.targetSample = targetSample;
    }
}
