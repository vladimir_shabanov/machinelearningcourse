import java.util.ArrayList;

/**
 * Created by Vladimir on 03.11.2016.
 */
public class ConfusionMatrix {
    private Classificator classificator;
    private ArrayList<Iris> testSample;
    private ArrayList<Iris> targetSample;

    private int[][] confusionMatrix;

    private void buildMatrix() {
        for (int i = 0; i < testSample.size(); i++) {
            confusionMatrix[targetSample.get(i).getIrisClass() - 1][testSample.get(i).getIrisClass() - 1]++;
        }
    }

    public ConfusionMatrix(Classificator classificator, ArrayList<Iris> testSample) {
        this.classificator = classificator;
        this.testSample = testSample;
        this.targetSample = classificator.getTargetSample();

        confusionMatrix = new int[3][3];

        buildMatrix();
    }

    public double getPrecision(int irisClass) {
        return (confusionMatrix[irisClass - 1][irisClass - 1] /
                (confusionMatrix[irisClass - 1][0] + confusionMatrix[irisClass - 1][1] + confusionMatrix[irisClass - 1][2]));
    }
    public double getRecall(int irisClass) {
        return (confusionMatrix[irisClass - 1][irisClass - 1] /
                (confusionMatrix[0][irisClass - 1] + confusionMatrix[1][irisClass - 1] + confusionMatrix[2][irisClass - 1]));
    }

    public void printInfo() {

        System.out.println("Info for " + classificator.getClass().toString().substring(6));
        System.out.println("Confusion matrix:");
        for (int i = 0; i < 3; i++){
            System.out.println(confusionMatrix[i][0] + " " + confusionMatrix[i][1] + " " + confusionMatrix[i][2]);
        }
        for (int i = 1; i <= 3; i++) {
            System.out.println("Precision for " + i + " class: " + getPrecision(i));
            System.out.println("Recall for " + i + " class: " + getRecall(i));
        }
    }


}
