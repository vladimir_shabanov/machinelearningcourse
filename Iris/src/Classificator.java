import java.util.ArrayList;

/**
 * Created by Vladimir on 02.11.2016.
 */
public interface Classificator {
    public void learn(ArrayList<Iris> learingSample);
    public void classificate(ArrayList<Iris> targetSample);
    public ArrayList<Iris> getTargetSample();
}
