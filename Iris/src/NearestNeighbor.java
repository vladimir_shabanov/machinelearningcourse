import java.util.ArrayList;

/**
 * Created by Vladimir on 01.11.2016.
 */
public class NearestNeighbor implements Classificator {
    private ArrayList<Iris> learingSample;
    private ArrayList<Iris> targetSample;

    @Override
    public ArrayList<Iris> getTargetSample() {
        return targetSample;
    }

    private double distance(Iris irisA, Iris irisB) {
        return Math.sqrt( (irisA.getPetalLength() - irisB.getPetalLength()) * (irisA.getPetalLength() - irisB.getPetalLength()) +
                (irisA.getPetalWidth() - irisB.getPetalWidth()) * (irisA.getPetalWidth() - irisB.getPetalWidth()) +
                (irisA.getSepalLength() - irisB.getSepalLength()) * (irisA.getSepalLength() - irisB.getSepalLength()) +
                (irisA.getSepalWidth() - irisB.getSepalWidth()) * (irisA.getSepalWidth() - irisB.getSepalWidth()) );
    }

    public NearestNeighbor() {

    }

    @Override
    public void learn(ArrayList<Iris> learingSample) {
        this.learingSample = learingSample;
    }

    @Override
    public void classificate(ArrayList<Iris> targetSample) {
        for (int i = 0; i < targetSample.size(); i++) {
            double minDistance = Double.MAX_VALUE;
            int index = 0;
            for (int j = 0; j < learingSample.size(); j++) {
                if (distance(targetSample.get(i), learingSample.get(j)) < minDistance) {
                    minDistance = distance(targetSample.get(i), learingSample.get(j));
                    index = j;
                }
            }
            targetSample.get(i).setIrisClass(learingSample.get(index).getIrisClass());
            //System.out.println(targetSample.get(i).getIrisClass());
        }
        this.targetSample = targetSample;
    }
}
