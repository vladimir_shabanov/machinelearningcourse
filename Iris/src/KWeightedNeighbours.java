import java.util.ArrayList;

/**
 * Created by Vladimir on 02.11.2016.
 */
public class KWeightedNeighbours extends KNearestNeighbours {

    public KWeightedNeighbours(int k) {
        this.k = k;
    }

    @Override
    int getAnswer(ArrayList<Iris> nearestNeighbours) {
        double twos = 0;
        double ones = 0;
        double threes = 0;
        for (int i = 0; i < nearestNeighbours.size(); i++) {
            if (nearestNeighbours.get(i).getIrisClass() == 1)
                ones += Math.pow(0.5, i);
            if (nearestNeighbours.get(i).getIrisClass() == 2)
                twos += Math.pow(0.5, i);
            if (nearestNeighbours.get(i).getIrisClass() == 3)
                threes += Math.pow(0.5, i);
        }

        if (ones > twos) {
            if (ones > threes)
                return 1;
            else
                return 3;
        } else {
            if (twos > threes)
                return 2;
            else
                return 3;
        }
    }

    @Override
    public void classificate(ArrayList<Iris> targetSample) {
        for (int i = 0; i < targetSample.size(); i++) {
            ArrayList<Iris> nearestNeighbours = getNearestNeighbours(targetSample.get(i));
            targetSample.get(i).setIrisClass(getAnswer(nearestNeighbours));
            //System.out.println(targetSample.get(i).getIrisClass());
        }
        this.targetSample = targetSample;
    }
}
