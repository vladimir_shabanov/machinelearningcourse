import java.io.*;
import java.util.ArrayList;

/**
 * Created by Vladimir on 01.11.2016.
 */
public class Main {
    private static ArrayList<Iris> learningSample;
    private static ArrayList<Iris> testSample;
    private static ArrayList<Iris> targetSample;

    private static int getClassByName(String str) {
        if (str.equals("Iris-setosa")) return 1;
        if (str.equals("Iris-versicolor")) return 2;
        if (str.equals("Iris-virginica")) return 3;
        return 0;
    }

    private static void fillSamples(String path) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
        learningSample = new ArrayList<Iris>();
        testSample = new ArrayList<Iris>();
        targetSample = new ArrayList<Iris>();

        String str;
        int i = 0;
        while ((str = bufferedReader.readLine()) != null) {
            String splitted[] = str.split(",");
            if ((i >= 0 && i < 40) || (i >= 50 && i < 90) || (i >= 100 && i < 140)) {

                learningSample.add(new Iris(Double.parseDouble(splitted[0]), Double.parseDouble(splitted[1]),
                        Double.parseDouble(splitted[2]), Double.parseDouble(splitted[3]), getClassByName(splitted[4])));
            } else {
                testSample.add(new Iris(Double.parseDouble(splitted[0]), Double.parseDouble(splitted[1]),
                        Double.parseDouble(splitted[2]), Double.parseDouble(splitted[3]), getClassByName(splitted[4])));
                targetSample.add(new Iris(Double.parseDouble(splitted[0]), Double.parseDouble(splitted[1]),
                        Double.parseDouble(splitted[2]), Double.parseDouble(splitted[3])));
            }
            i++;
        }

    }

    public static void main(String[] args) throws IOException {
        fillSamples("C:\\Users\\Vladimir\\IdeaProjects\\Iris\\data.txt");
/*
        NearestNeighbor nearestNeighbor = new NearestNeighbor();
        nearestNeighbor.learn(learningSample);
        nearestNeighbor.classificate(targetSample);

        KNearestNeighbours kNearestNeighbours = new KNearestNeighbours(3);
        kNearestNeighbours.learn(learningSample);
        kNearestNeighbours.classificate(targetSample);

        KWeightedNeighbours kWeightedNeighbours = new KWeightedNeighbours(3);
        kWeightedNeighbours.learn(learningSample);
        kWeightedNeighbours.classificate(targetSample);*/


        /*PotentialFunctionsMethod potentialFunctionsMethod = new PotentialFunctionsMethod();
        potentialFunctionsMethod.learn(learningSample);
        potentialFunctionsMethod.classificate(targetSample);

        ConfusionMatrix matrix = new ConfusionMatrix(potentialFunctionsMethod, testSample);
        matrix.printInfo();*/

/*        ParzenWindowMethod parzenWindowMethod = new ParzenWindowMethod(2);
        parzenWindowMethod.learn(learningSample);
        parzenWindowMethod.classificate(targetSample);*/


        /*LogisticRegression logisticRegression = new LogisticRegression(1, 0.5); //irisClass == 1, threshold = 0.5
        logisticRegression.learn(learningSample);
        logisticRegression.classificate(targetSample);*/

        ROC roc = new ROC(1, learningSample, targetSample, testSample);
        roc.calc();
    }
}
